import { Component, EventEmitter, Input, Output } from '@angular/core';
import {StatusService} from '../services/status.service';

@Component({
  selector: 'app-inactive-users',
  templateUrl: './inactive-users.component.html',
  styleUrls: ['./inactive-users.component.css']
})
export class InactiveUsersComponent {
  @Input() users: string[];
  @Output() userSetToActive = new EventEmitter<number>();

  constructor(private StatusService:StatusService){}

  onSetToActive(id: number,status:string) {
    // this.userSetToActive.emit(id);
    this.StatusService.setToActive(id);
  }
}
