import { Component,OnInit } from '@angular/core';
import {StatusService} from './services/status.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  activeUsers:{name:string,activeCount:number,inactiveCount:number}[] = [];
  inactiveUsers:{name:string,activeCount:number,inactiveCount:number}[] = [];

  constructor(private StatusService:StatusService){}

  ngOnInit() {
    this.activeUsers = this.StatusService.activeUsers;
    this.inactiveUsers = this.StatusService.inactiveUsers;
  }

  onSetToInactive(id: number) {
    this.inactiveUsers.push(this.activeUsers[id]);
    this.activeUsers.splice(id, 1);
  }

  onSetToActive(id: number) {
    this.activeUsers.push(this.inactiveUsers[id]);
    this.inactiveUsers.splice(id, 1);
  }
}
