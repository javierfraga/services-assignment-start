import { Component, EventEmitter, Input, Output } from '@angular/core';
import {StatusService} from '../services/status.service';

@Component({
  selector: 'app-active-users',
  templateUrl: './active-users.component.html',
  styleUrls: ['./active-users.component.css']
})
export class ActiveUsersComponent {
  @Input() users: string[];
  @Output() userSetToInactive = new EventEmitter<number>();

  constructor(private StatusService:StatusService){}

  onSetToInactive(id: number,status:string) {
    // this.userSetToInactive.emit(id);
    this.StatusService.setToInactive(id);
  }
}
