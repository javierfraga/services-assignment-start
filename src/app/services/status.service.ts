import {EventEmitter} from '@angular/core';

export class StatusService {

	activeUsers = [
		{
			name:'Max',
			activeCount:1,
			inactiveCount:0,
		},
		{
			name:'Anna',
			activeCount:1,
			inactiveCount:0,  		
		},
	];
	inactiveUsers = [
		{
			name:'Chris',
			activeCount:0,
			inactiveCount:1,
		},
		{
			name:'Manu',
			activeCount:0,
			inactiveCount:1,  		
		},
	];

	constructor() {}

	setToActive(id:number) {
		this.inactiveUsers[id].activeCount++;
		this.activeUsers.push(this.inactiveUsers[id]);
		this.inactiveUsers.splice(id, 1);	
	}
	setToInactive(id:number) {
		this.activeUsers[id].inactiveCount++;
		this.inactiveUsers.push(this.activeUsers[id]);
		this.activeUsers.splice(id, 1);
	}

}
